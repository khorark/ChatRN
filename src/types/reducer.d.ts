export interface IReduxState {
    messages: IMessage[],
    isLoading: boolean,
}

export interface IMessage {
    id: number,
    author: number,
    text: string,
    date: Date,
}
