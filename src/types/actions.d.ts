import { Action, AnyAction } from 'redux'
import { Type } from '../redux/actions'
import { IMessage } from './reducer'

export type IDispatch<A extends Action = AnyAction> = <T extends A>(action: T) => T;

export interface IGetMessages extends Action {
    type: Type.GET_MESSAGES
    payload: any,
}

export interface IAddMessage extends Action {
    type: Type.ADD_MESSAGE
    message: {
        id: number
        author: number,
        text: string,
        date: Date,
    },
}

export interface IRemoveMessage extends Action {
    type: Type.REMOVE_MESSAGE
    id: number,
}

export interface ILoading extends Action {
    type: Type.IS_LOADING,
    isLoading: boolean,
}

export interface IAddMessagePayload {
    message: IMessage,
}

export interface IRemoveMessagePayload {
    id: number,
}

export interface IReduxActions {
    getMessages: any,
    addMessage: any,
    removeMessage: any,
}

export type ChatActions = IGetMessages | IAddMessage | IRemoveMessage | ILoading
