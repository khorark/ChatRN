import { applyMiddleware, compose, createStore, Store } from 'redux'
import thunk from 'redux-thunk'

import rootReducer from './reducer'
import { IReduxState } from '../types/reducer'

const state = {
    messages: [],
    isLoading: false,
}

export default (initialState = state): Store<IReduxState> => {
    // @ts-ignore
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

    const store = createStore(rootReducer, initialState, composeEnhancers(applyMiddleware(thunk)))

    // @ts-ignore
    if (module) {
        // @ts-ignore
        module.hot.accept(() => {
            const nextRootReducer = require('./reducer').default
            store.replaceReducer(nextRootReducer)
        })
    }

    return store
}
