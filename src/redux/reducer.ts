import { ChatActions } from '../types/actions'
import { IReduxState } from '../types/reducer'
import { Type } from './actions'
import { Reducer } from 'redux'

const reducer: Reducer<IReduxState | undefined, ChatActions> = (
    state: IReduxState | undefined,
    action: ChatActions,
) => {
    let messages
    if (!state) {
        return undefined
    }
    switch (action.type) {
        case Type.GET_MESSAGES:
            return { ...state }
        case Type.ADD_MESSAGE:
            messages = [...state.messages]
            messages.push(action.message)
            return { ...state, messages }
        case Type.REMOVE_MESSAGE:
            messages = [...state.messages].filter(({ id }) => id !== action.id)
            return { ...state, messages }
        case Type.IS_LOADING:
            return { ...state, isLoading: action.isLoading }
        default:
            return state
    }
}

export default reducer
