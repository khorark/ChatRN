/**
 * Created by arkadiy on 06.09.18.
 */
import { ActionCreator, ActionCreatorsMapObject } from 'redux'
import { ThunkAction } from 'redux-thunk'
import {
    IAddMessage,
    IAddMessagePayload,
    IGetMessages,
    ILoading,
    IRemoveMessage,
    IRemoveMessagePayload,
} from '../types/actions'
import { IReduxState } from '../types/reducer'

export enum Type {
    GET_MESSAGES = '@@chat/GET_MESSAGES',
    ADD_MESSAGE = '@@chat/ADD_MESSAGE',
    REMOVE_MESSAGE = '@@chat/REMOVE_MESSAGE',
    IS_LOADING = '@@chat/IS_LOADING',
}

export const getMessages = (): ThunkAction<void, IReduxState, null, IGetMessages | ILoading> => dispatch => {
    dispatch(changeLoadingStatus(true))
    setTimeout(() => {
        dispatch(changeLoadingStatus(false))
        dispatch({
            payload: {},
            type: Type.GET_MESSAGES,
        })
    }, 2000)
}

export const addMessage = ({
    message,
}: IAddMessagePayload): ThunkAction<void, IReduxState, null, IAddMessage | ILoading> => dispatch => {
    dispatch(changeLoadingStatus(true))
    setTimeout(() => {
        dispatch(changeLoadingStatus(false))
        dispatch({
            message,
            type: Type.ADD_MESSAGE,
        })
    }, 2000)
}

export const removeMessage = ({
    id,
}: IRemoveMessagePayload): ThunkAction<void, IReduxState, null, IRemoveMessage | ILoading> => (dispatch: any) => {
    dispatch(changeLoadingStatus(true))
    setTimeout(() => {
        dispatch(changeLoadingStatus(false))
        dispatch({
            id,
            type: Type.REMOVE_MESSAGE,
        })
    }, 2000)
}

export const changeLoadingStatus: ActionCreator<ILoading> = (isLoading: boolean) => ({
    type: Type.IS_LOADING,
    isLoading,
})

export const Actions: ActionCreatorsMapObject = {
    getMessages,
    addMessage,
    removeMessage,
    changeLoadingStatus,
}
