/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React from 'react'
import { PureComponent } from 'react'
import { Provider } from 'react-redux'

import createStore from './redux/store'
import { AppNavigator } from './navigator/AppNavigator'

const store = createStore()

export default class App extends PureComponent {
    public render() {
        return (
            <Provider store={store}>
                <AppNavigator />
            </Provider>
        )
    }
}
