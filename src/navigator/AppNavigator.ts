import { createStackNavigator, createSwitchNavigator } from 'react-navigation'
import AuthLoadingScreen from '../components/AuthLoadingScreen'
import Login from '../components/Login'
import Register from '../components/Register'
import Chat from '../components/Chat'
import ValidateScreen from '../components/ValidateScreen'

const AppNavigatorStack = createStackNavigator(
    {
        LoginScreen: { screen: Login },
        ValidateScreen: { screen: ValidateScreen },
        ChatScreen: { screen: Chat },
    },
    {
        mode: 'modal',
        navigationOptions: {
            header: null,
        },
    },
)

export const AppNavigator = createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        App: AppNavigatorStack,
        Auth: Register,
    },
    {
        initialRouteName: 'AuthLoading',
    },
)
