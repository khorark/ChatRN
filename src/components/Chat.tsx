import React, { PureComponent } from 'react'
import {
    ScrollView,
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    AppState,
    TextInput,
    NativeSyntheticEvent,
    TextInputChangeEventData,
    PixelRatio,
    Keyboard,
    BackHandler,
} from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { IMessage, IReduxState } from '../types/reducer'
import { Actions } from '../redux/actions'
import { IDispatch, IReduxActions } from '../types/actions'
import { LoadingIndicator } from './LoadingScreen'

interface IChatProps {
    navigation: any
    messages: IMessage[]
    isLoading: boolean
    actions: IReduxActions
}

interface IChatState {
    messageText: string
}

const mapStateToProps = (state: IReduxState) => ({
    messages: state.messages,
    isLoading: state.isLoading,
})

const mapDispatchToProps = (dispatch: IDispatch) => ({
    actions: bindActionCreators(Actions, dispatch),
})

// @ts-ignore
@connect(
    mapStateToProps,
    mapDispatchToProps,
)
export default class Chat extends PureComponent<IChatProps, IChatState> {
    public state = {
        messageText: '',
    }

    public componentDidMount() {
        this.props.actions.getMessages()
        AppState.addEventListener('change', this.onAppStateChange)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }

    public componentWillUnmount() {
        AppState.removeEventListener('change', this.onAppStateChange)
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    public renderMessage = ({ id, text }: IMessage) => {
        return (
            <View key={id} style={styles.messageContainer}>
                <Image source={require('../static/img/avatar/ava.jpg')} style={styles.avatarContainer} />
                <View style={styles.inputContainer}>
                    <Text style={styles.message}>{text}</Text>
                    <TouchableOpacity style={styles.trashContainer} onPress={this.handleRemoveMessage.bind(this, id)}>
                        <Image source={require('../static/img/trash/trash_can.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    public render() {
        return (
            <>
                {this.props.isLoading ? <LoadingIndicator /> : null}
                <View style={styles.mainContainer}>
                    <ScrollView style={styles.chatContainer} contentContainerStyle={styles.chatContainer}>
                        {this.props.messages.map(message => this.renderMessage(message))}
                    </ScrollView>
                    <View style={styles.inputMessageContainer}>
                        <Image source={require('../static/img/avatar/ava.jpg')} style={styles.avatarContainer} />
                        <View style={styles.inputContainer}>
                            <ScrollView keyboardShouldPersistTaps='always' keyboardDismissMode='on-drag'>
                                <TextInput
                                    style={styles.input}
                                    multiline={true}
                                    placeholder={'Напишите что-то...'}
                                    value={this.state.messageText}
                                    onChange={this.handleOnChangeText}
                                    underlineColorAndroid='transparent'
                                />
                            </ScrollView>
                            <TouchableOpacity style={styles.sendContainer} onPress={this.handleSendMessage}>
                                <Image
                                    source={require('../static/img/send/send.png')}
                                    style={{ tintColor: this.state.messageText === '' ? '#d8d8d8' : undefined }}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </>
        )
    }

    private onAppStateChange = (currentAppState: string) => {
        if (currentAppState === 'active') {
            this.props.navigation.navigate('LoginScreen')
        }
    }

    private handleOnChangeText = (e: NativeSyntheticEvent<TextInputChangeEventData>) => {
        this.setState({ messageText: e.nativeEvent.text })
    }

    private handleRemoveMessage = (id: number) => {
        this.props.actions.removeMessage({ id })
    }

    private handleSendMessage = () => {
        Keyboard.dismiss()
        if (!this.state.messageText) { return false }
        const lastMessage = [...this.props.messages].pop() || { id: undefined }
        const lastId = (lastMessage && lastMessage.id) || 0
        const message = {
            id: lastId + 1,
            author: 1,
            text: this.state.messageText,
            date: new Date(),
        }

        this.props.actions.addMessage({ message })
        this.setState({ messageText: '' })
    }

    private handleBackPress = () => true
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#ebebeb',
        paddingHorizontal: 16,
        paddingVertical: 17,
    },
    chatContainer: {
        flexGrow: 1,
    },
    messageContainer: {
        flexDirection: 'row',
        marginBottom: 20,
    },
    message: {
        fontSize: 15,
        fontWeight: '200',
        color: '#000',
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRightWidth: 1,
        borderRightColor: '#d8d8d8',
    },
    inputMessageContainer: {
        flexDirection: 'row',
    },
    inputContainer: {
        flex: 1,
        borderRadius: 22,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#d8d8d8',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 1,
    },
    avatarContainer: {
        borderRadius: (44 * PixelRatio.get()) / 2,
        marginRight: 16,
        marginTop: 5,
    },
    input: {
        minHeight: 44,
        maxHeight: 200,
        flex: 1,
        paddingVertical: 5,
        borderRightWidth: 1,
        borderRightColor: '#d8d8d8',
    },
    sendContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        height: '90%',
        paddingLeft: 10,
        paddingBottom: 10,
        // borderLeftWidth: 1,
        // borderLeftColor: '#d8d8d8',
    },
    trashContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        height: '90%',
        paddingLeft: 10,
        paddingTop: 5,
    },
})
