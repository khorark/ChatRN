import React, { PureComponent } from 'react'
import { StyleSheet, Image } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

export interface IValidateScreenProps {
    navigation: any
}

export default class ValidateScreen extends PureComponent<IValidateScreenProps> {
    public componentDidMount() {
        const { params } = this.props.navigation.state
        const isSuccess = params && params.isSuccess
        setTimeout(() => {
            if (isSuccess) {
                this.props.navigation.navigate('ChatScreen')
            } else {
                this.props.navigation.navigate('LoginScreen')
            }
        }, 2000)
    }

    public renderSuccessScreen = () => (
        <LinearGradient colors={['#32ccbc', '#90f7ec']} style={styles.mainContainer}>
            <Image source={require('../static/img/success.png')} style={styles.iconContainer} />
        </LinearGradient>
    )

    public renderErrorScreen = () => (
        <LinearGradient colors={['#32ccbc', '#f790e7']} style={styles.mainContainer}>
            <Image source={require('../static/img/error.png')} style={styles.iconContainer} />
        </LinearGradient>
    )

    public render() {
        const { params } = this.props.navigation.state
        const isSuccess = params && params.isSuccess
        return isSuccess ? this.renderSuccessScreen() : this.renderErrorScreen()
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    iconContainer: {
        width: 182,
        height: 182,
    },
})
