import React from 'react'
import { View, StyleSheet, ActivityIndicator } from 'react-native'

export const LoadingIndicator = () => {
    return (
        <View style={styles.loadingContainer}>
            <ActivityIndicator color='#32ccbc' size={'large'} />
        </View>
    )
}

const styles = StyleSheet.create({
    loadingContainer: {
        position: 'absolute',
        backgroundColor: '#fff',
        opacity: 0.5,
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        zIndex: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
})
