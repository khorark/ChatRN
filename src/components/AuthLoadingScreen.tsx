import React, { PureComponent } from 'react'
import { AsyncStorage } from 'react-native'
import { LoadingIndicator } from './LoadingScreen'

export interface IAuthLoadingProps {
    navigation: any,
}

export default class AuthLoadingScreen extends PureComponent<IAuthLoadingProps> {
    public componentDidMount() {
        this.getStartState()
    }

    public render() {
        return <LoadingIndicator/>
    }

    private getStartState = async () => {
        try {
            // await AsyncStorage.removeItem('@PIN')
            const value = await AsyncStorage.getItem('@PIN')
            if (value) {
                this.props.navigation.navigate('App')
            } else {
                this.props.navigation.navigate('Auth')
            }
        } catch (e) {
            this.props.navigation.navigate('Auth')
            console.warn('Error get Pin code ', e)
        }
    }
}
