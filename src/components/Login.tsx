import React, { PureComponent } from 'react'
import { ScrollView, View, StyleSheet, Text, TouchableOpacity, Image, AsyncStorage } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import LogoIcon from '../static/img/LogoIcon'

export interface ILoginProps {
    navigation: any
}

export interface ILoginState {
    pin: string
}

export default class Login extends PureComponent<ILoginProps, ILoginState> {
    public state = {
        pin: '',
    }

    public componentDidUpdate() {
        if (this.state.pin.length === 4) { this.checkValidPin() }
    }

    public render() {
        return (
            <ScrollView style={styles.scrollViewContainer} contentContainerStyle={styles.scrollViewContainer}>
                <LinearGradient colors={['#32ccbc', '#90f7ec']} style={styles.mainContainer}>
                    <View style={styles.imageContainer}>
                        <LogoIcon />
                    </View>
                    <View style={styles.centerContainer}>
                        <View style={styles.inputContainer}>
                            {Array.from(Array(4).keys()).map((key: number) => (
                                <View
                                    key={key}
                                    style={[styles.circle, this.state.pin.length > key ? styles.circleFill : null]}
                                />
                            ))}
                        </View>
                        <View style={styles.containerNumPad}>
                            {Array.from(Array(9).keys()).map((key: number) => (
                                <TouchableOpacity
                                    key={key}
                                    style={styles.numberContainer}
                                    onPress={this.handleChangePin.bind(this, key)}
                                >
                                    <Text style={styles.numberText}>{key + 1}</Text>
                                </TouchableOpacity>
                            ))}
                            <View style={styles.bottomNumPad}>
                                <TouchableOpacity
                                    style={styles.numberContainer}
                                    onPress={this.handleChangePin.bind(this, -1)}
                                >
                                    <Text style={styles.numberText}>0</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.deleteContainer}
                                    onPress={this.handleChangePin.bind(this, -2)}
                                >
                                    <Image
                                        source={require('../static/img/delete_1.png')}
                                        style={{ width: 45, height: 33 }}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </LinearGradient>
            </ScrollView>
        )
    }

    private handleChangePin = (key: number) => {
        switch (key) {
            case -2:
                this.setState({ pin: this.state.pin.slice(0, this.state.pin.length - 1) })
                break
            default:
                if (this.state.pin.length !== 4) {
                    this.setState({ pin: this.state.pin + (key + 1) })
                }
                break
        }
    }

    private checkValidPin = async () => {
        try {
            const value = await AsyncStorage.getItem('@PIN')
            const isSuccess = value === this.state.pin
            this.props.navigation.navigate('ValidateScreen', { isSuccess })
            this.setState({ pin: '' })
        } catch (e) {
            console.warn('Error get Pin code ', e)
        }
    }
}

const styles = StyleSheet.create({
    scrollViewContainer: {
        flexGrow: 1,
    },
    mainContainer: {
        flex: 1,
        alignItems: 'center',
    },
    imageContainer: {
        marginBottom: '15%',
        marginLeft: '-15%',
        marginTop: '5%',
        transform: [{ scale: 0.75 }],
    },
    centerContainer: {
        alignItems: 'center',
        width: 400,
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 30,
    },
    circle: {
        width: 14,
        height: 14,
        borderRadius: 7,
        borderWidth: 2,
        borderColor: '#fff',
        marginHorizontal: 14,
    },
    circleFill: {
        backgroundColor: '#fff',
    },
    containerNumPad: {
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
    bottomNumPad: {
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'relative',
        flexGrow: 1,
    },
    deleteContainer: {
        position: 'absolute',
        top: 35,
        right: 65,
        // marginLeft: 130,
    },
    numberContainer: {
        width: 77,
        height: 77,
        borderRadius: 39,
        borderWidth: 2,
        borderColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 20,
        marginVertical: 10,
    },
    numberText: {
        fontSize: 36.5,
        fontWeight: '100',
        color: '#fefefe',
    },
})
