import React, { PureComponent } from 'react'
import {
    View,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    AsyncStorage,
    NativeSyntheticEvent,
    ScrollView,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import OpenEyeIcon from '../static/img/OpenEyeIcon'
import LogoIcon from '../static/img/LogoIcon'

interface IRegisterProps {
    navigation: any
}

interface IRegisterState {
    securityText: boolean
}

export default class Register extends PureComponent<IRegisterProps, IRegisterState> {
    public state = {
        securityText: true,
    }

    public render() {
        return (
            <ScrollView style={styles.scrollViewContainer} contentContainerStyle={styles.scrollViewContainer}>
                <LinearGradient colors={['#32ccbc', '#90f7ec']} style={styles.mainContainer}>
                    <View style={styles.imageContainer}>
                        <LogoIcon/>
                    </View>
                    <View style={{ marginBottom: '20%' }}>
                        <View style={styles.inputContainer}>
                            <TextInput
                                style={styles.input}
                                keyboardType={'numeric'}
                                maxLength={4}
                                textContentType={'password'}
                                onEndEditing={this.storeData}
                                secureTextEntry={this.state.securityText}
                                underlineColorAndroid='transparent'
                            />
                            <TouchableOpacity style={styles.eyeContainer} onPress={this.handleToggleHideMode}>
                                <OpenEyeIcon/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </LinearGradient>
            </ScrollView>
        )
    }

    private storeData = async (e: NativeSyntheticEvent<{ text: string }>) => {
        const value = e.nativeEvent.text
        if (!value || value.length !== 4) {return false}
        try {
            await AsyncStorage.setItem('@PIN', value)
            this.props.navigation.navigate('ChatScreen')
        } catch (error) {
            console.error('Error saving data: ', error)
        }
    }

    private handleToggleHideMode = (): void => this.setState({ securityText: !this.state.securityText })
}

const styles = StyleSheet.create({
    scrollViewContainer: {
        flexGrow: 1,
    },
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputContainer: {
        width: 315,
        height: 44,
        borderRadius: 22,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#d8d8d8',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 1,
    },
    input: {
        flex: 1,
    },
    eyeContainer: {
        borderLeftWidth: 1,
        borderLeftColor: '#d8d8d8',
        paddingLeft: 10,
    },
    imageContainer: {
        marginBottom: '20%',
        transform: [{ scale: 1.36 }],
        marginLeft: '-40%',
    },
})
